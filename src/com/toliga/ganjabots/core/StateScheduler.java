package com.toliga.ganjabots.core;

import java.util.HashMap;

import org.dreambot.api.script.AbstractScript;

public class StateScheduler {
    private boolean isStarted;
    private State nextState;
    private AbstractScript context;
    private HashMap<String, Object> settings;

    public StateScheduler(AbstractScript context, HashMap<String, Object> settings, State startState) {
        this.nextState = startState;
        this.context = context;
        this.settings = settings;
    }

    public void executeState(AntibanManager antibanManager) {
    	boolean isChangeState = nextState.execute(this.context, this.settings, antibanManager);
    	
        if (isChangeState) {
        	this.settings.put("state", this.nextState.stateName);
            AbstractScript.log("Ganja Combat Bot " + this.settings.get("version") + ": Current State: **" + this.settings.get("state") + "**");
            nextState = nextState.next();
            
        	this.settings.put("state", this.nextState.stateName);
            AbstractScript.log("Ganja Combat Bot " + this.settings.get("version") + ": State Transition To: **" + this.settings.get("state") + "**");
        }
    }

    public void setStarted(boolean isStarted) {
        this.isStarted = isStarted;
    }

    public boolean isStarted() {
        return isStarted;
    }
}
