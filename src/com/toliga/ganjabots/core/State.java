package com.toliga.ganjabots.core;

import java.util.HashMap;

import org.dreambot.api.script.AbstractScript;

public abstract class State {
	public String stateName;
	protected State nextState;
	
	public State(String stateName) {
		this.stateName = stateName;
	}
	
    public abstract boolean execute(AbstractScript context, HashMap<String, Object> settings, AntibanManager antibanManager);
    
    public State next() {
    	return nextState;
    }
}
